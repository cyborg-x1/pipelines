 #!/bin/sh
rm -rf build
cmake -E make_directory build
cmake -E chdir build cmake .. -DQUARTUS_NUM_PARALLEL_PROCESSORS=4
cd build
make TimeCounter_SVF VERBOSE=1
#cmake --build build --target TimeCounter_SVF
cd ..
