containerId="example"
containerImage="image"
echo "RUN"
docker run \
       -td \
       --name \
       $containerId \
       --env="DISPLAY" \
       --env="QT_X11_NO_MITSHM=1" \
       --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
       --volume="/tmp/pulse-socket:/tmp/pulse-socket" \
       --volume="/etc/localtime:/etc/localtime:ro" \
       -w "/install" \
       $containerImage bash

echo "GRANT"
hostname=`docker inspect --format='{{ .Config.Hostname }}' $containerId`
xhost +local:$hostname

echo "EXEC"
docker exec -it $containerId bash

echo "REVOKE"
xhost -local:$hostname
docker stop $containerId
